# Sudoku Solver
Sudoku solver written in Python 3 using simple deduction and minimal backtracking.
Works on simple input grids without guessing, on harder sudoku's guessing is required. 
### Example usage
##### From numpy array
```python
import numpy as np
from sudoku_solver.sudoku import Sudoku
s = Sudoku(np.array(
        [[5, 3, 0, 0, 7, 0, 0, 0, 0],
         [6, 0, 0, 1, 9, 5, 0, 0, 0],
         [0, 9, 8, 0, 0, 0, 0, 6, 0],
         [8, 0, 0, 0, 6, 0, 0, 0, 3],
         [4, 0, 0, 8, 0, 3, 0, 0, 1],
         [7, 0, 0, 0, 2, 0, 0, 0, 6],
         [0, 6, 0, 0, 0, 0, 2, 8, 0],
         [0, 0, 0, 4, 1, 9, 0, 0, 5],
         [0, 0, 0, 0, 8, 0, 0, 7, 9]]))
s.solve()
s.print_grid()
```
##### From text file
[Example text file](tests/test_input.txt)
```python
from sudoku_solver.sudoku import Sudoku
s = Sudoku('tests/test_input.txt')
s.solve()
s.print_grid()
```

#### Image parser
Very basic optical character recognition class to retrieve numpy gird based onwhite background with black lines and numbers.
Images needs to be cropped to the exact size of the  9x9 grid, where the outer pixels are black.

![alt text](tests/sudoku.png "Example image")
##### From image
```python
import numpy as np
from sudoku_solver.sudoku import Sudoku
from sudoku_solver.image_parser import SudokuImage
s_img = SudokuImage('tests/sudoku.png')
s = Sudoku(s_img.parse())
s.solve()
s.print_grid()
```
## To do
1. ~~Docstrings for image parser~~
2. ~~Additional information for image parser training data~~
3. Fix bug where some 17 hints Sudokus are not solved correctly
4. Better unit testing
5. Check and if necessary improve 'solve_grid' method (maybe Cython?)
