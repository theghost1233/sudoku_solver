try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Sudoku solver',
    'author': 'Casper Meijer',
    'url': 'https://gitlab.com/theghost1233/sudoku_solver',
    'author_email': 'c.meijer.nl@gmail.com',
    'version': '1.0',
    'install_requires': ['nose'],
    'packages': ['numpy', 'skimage', 'sklearn'],
    'scripts': [],
    'name': 'Sudoku Solver'
}

setup(**config)
