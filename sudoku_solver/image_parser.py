#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 15 15:37:19 2017

@author: casper
"""
import numpy as np
import pickle
from skimage import color, io
from sklearn import svm


class SudokuImage():
    """Optical character recognition of 9x9 Sudoku.

    Args:
        path (str): String with the path to the image file

    Attributes:
        image_data (nda): grayscale values for each pixel
        r_dif (nda): row divider index's
        c_dif (nda): column divider index's
        cellls (list): list with numpy array of pixel values for each
                      Sudoku cell

    Usage:
        Before using the image parser you need to have training data
        for the classifier and prepare 	the image file. The image file
        needs to be on a white background with black divider lines and
        numbers. The outermost pixels need to be black and describe
        the edge of the Sudoku.

    Example:
        s_img = SudokuImage('tests/sudoku.png')
        s = Sudoku(s_img.parse())
    """

    def __init__(self, path):
        """Initalize an instance of the SudokuImage"""
        self.image_data = np.array(color.rgb2gray(io.imread(path)))

    def parse(self):
        """Parse the image and return the numbers for each cell

        First find the divider lines, including outer edges. Next
        use these to divide the grid into individual cells. For each
        cell remove all the whitespace so that the edges of the
        numbers align with the edge of the cell image. Resize the
        cell images to the same size as the training data using
        linear interpolation. Next train the classifier and run
        all the cells trough it. Reshape the array before returning.
        """
        self.find_grid()
        self.find_cells()
        self.clean_cells()
        self.clean_cells(trim=False)
        self.resize_cells()
        self.train_model()
        self.find_number()
        self.grid = np.array(self.number).reshape([9, 9])
        return self.grid

    def find_grid(self, cut_off=0.4):
        """Find indices's of the divider lines

        Find all indices's of for the divider lines based on the
        gradient. Assume the divider lines run over the whole length
        of the image.
        """
        row_lines = []
        column_lines = []
        for row in range(len(self.image_data[:, 0])):
            if (self.image_data[row, :] < cut_off).all():
                row_lines.append(row)
        for column in range(len(self.image_data[0, :])):
            if (self.image_data[:, column] < cut_off).all():
                column_lines.append(column)
        row_lines = np.array(row_lines)
        column_lines = np.array(column_lines)
        row_divide = row_lines[np.append(True, np.diff(row_lines) > 1)]
        column_divide = column_lines[np.append(True,
                                               np.diff(column_lines) > 1)]
        self.r_dif = row_divide
        self.c_dif = column_divide

    def find_cells(self):
        """Split the  full grid into individual cells

        Loop trough all row and column dividers and store the pixel
        values for each individual cell in a list.
        """
        cells = []
        EXPECTED_CELLS = 81
        for r in range(len(self.r_dif) - 1):
            for c in range(len(self.c_dif) - 1):
                r_index = self.r_dif[r]
                r_plus_index = self.r_dif[r + 1]
                c_index = self.c_dif[c]
                c_plus_index = self.c_dif[c + 1]
                cells.append(self.image_data[r_index:r_plus_index,
                                             c_index:c_plus_index])
        if len(cells) == EXPECTED_CELLS:
            self.cells = cells
        else:
            raise ValueError('Could not find all cells')

    def clean_cells(self, max_var=0.1, trim=True):
        """Remove whitespace from the outer edges.

        Find row's and columns with only one color and remove those.
        """
        new_cell = []
        for cell in self.cells:
            if (cell == 1).all():
                new_cell.append(cell)
            else:
                row_lines = []
                column_lines = []
                for row in range(len(cell[:, 0])):
                    if np.std(cell[row, :], ddof=1) < max_var:
                        row_lines.append(row)
                for column in range(len(cell[0, :])):
                    if np.std(cell[:, column], ddof=1) < max_var:
                        column_lines.append(column)
                cell = np.delete(cell, row_lines, 0)
                cell = np.delete(cell, column_lines, 1)
                if trim:
                    rows = len(cell[:, 0])
                    columns = len(cell[0, :])
                    cell = np.delete(cell, np.array([0, rows - 1]), 0)
                    cell = np.delete(cell, np.array([0, columns - 1]), 1)
                new_cell.append(cell)
        self.cells = new_cell

    def resize_cells(self, size=16):
        """Resize cell's trough linear interpolation."""
        new_cell = []
        for cell in self.cells:
            x = np.linspace(0, 1, len(cell[0, :]))
            x_int = np.linspace(0, 1, size)
            y_int = []
            for y in cell:
                y_int.append(np.interp(x_int, x, y))
            temp_cell = np.array(y_int)

            x = np.linspace(0, 1, len(temp_cell[:, 0]))
            out = np.zeros([size, size])
            for y in range(len(temp_cell[0, :])):
                out[:, y] = np.interp(x_int, x, temp_cell[:, y])
            new_cell.append(out)
        self.cells = new_cell

    def train_model(self):
        """Train the SVM algorithm

        More information about the training data will follow.
        Training data is expected in a dictionary called 'data'
        data:
            test (list): N element list with each a 1x256 numpy array
                         representing the 16x16 digit pixel values
            target (list): N element list with the correct digits
        """
        clf = svm.SVC(C=10, gamma=0.001)
        data = pickle.load(open('training_data/test_data', 'rb'))
        clf.fit(data['test'], data['target'])
        self.clf = clf

    def find_number(self):
        """Find the digit representation of each cell."""
        number = []
        # Loop becasue feeding everything would cause issues with empty cells
        for cell in self.cells:
            if (cell == 1).all():
                number.append(int(0))
            else:
                test = np.reshape(cell, [np.size(cell)])
                number.append(int(self.clf.predict(test.reshape(1, -1))))
        self.number = number
