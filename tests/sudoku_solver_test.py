from nose.tools import assert_equal, raises, assert_true, assert_false
from sudoku_solver.sudoku import Sudoku
from sudoku_solver.image_parser import SudokuImage

import numpy as np


def test_sudoku_import_file():
    sudo_file = Sudoku("tests/test_input.txt")
    sudo_ref = np.array(
        [[0, 0, 3, 0, 2, 0, 6, 0, 0],
         [9, 0, 0, 3, 0, 5, 0, 0, 1],
         [0, 0, 1, 8, 0, 6, 4, 0, 0],
         [0, 0, 8, 1, 0, 2, 9, 0, 0],
         [7, 0, 0, 0, 0, 0, 0, 0, 8],
         [0, 0, 6, 7, 0, 8, 2, 0, 0],
         [0, 0, 2, 6, 0, 9, 5, 0, 0],
         [8, 0, 0, 2, 0, 3, 0, 0, 9],
         [0, 0, 5, 0, 1, 0, 3, 0, 0]])
    assert_true((sudo_file.grid == sudo_ref).all())
    assert_equal(sudo_file.length, 9)
    assert_equal(sudo_file.bu_grid, [])
    assert_equal(sudo_file.guesses, 0)


def test_sudoku_import_array():
    sudo_array = Sudoku(np.array(
        [[0, 0, 3, 0, 2, 0, 6, 0, 0],
         [9, 0, 0, 3, 0, 5, 0, 0, 1],
         [0, 0, 1, 8, 0, 6, 4, 0, 0],
         [0, 0, 8, 1, 0, 2, 9, 0, 0],
         [7, 0, 0, 0, 0, 0, 0, 0, 8],
         [0, 0, 6, 7, 0, 8, 2, 0, 0],
         [0, 0, 2, 6, 0, 9, 5, 0, 0],
         [8, 0, 0, 2, 0, 3, 0, 0, 9],
         [0, 0, 5, 0, 1, 0, 3, 0, 0]]))
    sudo_ref = np.array(
        [[0, 0, 3, 0, 2, 0, 6, 0, 0],
         [9, 0, 0, 3, 0, 5, 0, 0, 1],
         [0, 0, 1, 8, 0, 6, 4, 0, 0],
         [0, 0, 8, 1, 0, 2, 9, 0, 0],
         [7, 0, 0, 0, 0, 0, 0, 0, 8],
         [0, 0, 6, 7, 0, 8, 2, 0, 0],
         [0, 0, 2, 6, 0, 9, 5, 0, 0],
         [8, 0, 0, 2, 0, 3, 0, 0, 9],
         [0, 0, 5, 0, 1, 0, 3, 0, 0]])
    assert_true((sudo_array.grid == sudo_ref).all())
    assert_equal(sudo_array.length, 9)
    assert_equal(sudo_array.bu_grid, [])
    assert_equal(sudo_array.guesses, 0)


@raises(ValueError)
def test_sudoku_import_error():
    Sudoku(range(9))


def test_sudoku_check_partial_true():
    sudo = Sudoku(np.array(
        [[0, 0, 3, 0, 2, 0, 6, 0, 0],
         [9, 0, 0, 3, 0, 5, 0, 0, 1],
         [0, 0, 1, 8, 0, 6, 4, 0, 0],
         [0, 0, 8, 1, 0, 2, 9, 0, 0],
         [7, 0, 0, 0, 0, 0, 0, 0, 8],
         [0, 0, 6, 7, 0, 8, 2, 0, 0],
         [0, 0, 2, 6, 0, 9, 5, 0, 0],
         [8, 0, 0, 2, 0, 3, 0, 0, 9],
         [0, 0, 5, 0, 1, 0, 3, 0, 0]]))
    assert_true(sudo.is_valid())


def test_sudoku_check_partial_false():
    sudo = Sudoku(np.array(
        [[0, 0, 3, 0, 2, 0, 6, 0, 0],
         [9, 0, 0, 3, 0, 5, 0, 0, 1],
         [0, 0, 1, 8, 0, 6, 4, 0, 1],
         [0, 0, 8, 1, 0, 2, 9, 0, 0],
         [7, 0, 0, 0, 0, 0, 0, 0, 8],
         [0, 0, 6, 7, 0, 8, 2, 0, 0],
         [0, 0, 2, 6, 0, 9, 5, 0, 0],
         [8, 0, 0, 2, 0, 3, 0, 0, 9],
         [0, 0, 5, 0, 1, 0, 3, 0, 0]]))
    assert_false(sudo.is_valid())


def test_sudoku_horizontal():
    sudo_test = Sudoku(np.array(
        [[0, 2, 3, 4, 5, 6, 7, 8, 9],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0]]))
    sudo_test.update_mask()
    sudo_test.fill_grid()
    sudo_ref = np.array(
        [[1, 2, 3, 4, 5, 6, 7, 8, 9],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0]])
    print(sudo_test.grid)
    assert_true((sudo_test.grid == sudo_ref).all())


def test_sudoku_vertical():
    sudo_test = Sudoku(np.array(
        [[0, 0, 0, 4, 0, 0, 0, 0, 0],
         [0, 0, 0, 5, 0, 0, 0, 0, 0],
         [0, 0, 0, 6, 0, 0, 0, 0, 0],
         [0, 0, 0, 7, 0, 0, 0, 0, 0],
         [0, 0, 0, 8, 0, 0, 0, 0, 0],
         [0, 0, 0, 9, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 0, 0, 0, 0, 0],
         [0, 0, 0, 2, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0]]))
    sudo_test.update_mask()
    sudo_test.fill_grid()
    sudo_ref = np.array(
        [[0, 0, 0, 4, 0, 0, 0, 0, 0],
         [0, 0, 0, 5, 0, 0, 0, 0, 0],
         [0, 0, 0, 6, 0, 0, 0, 0, 0],
         [0, 0, 0, 7, 0, 0, 0, 0, 0],
         [0, 0, 0, 8, 0, 0, 0, 0, 0],
         [0, 0, 0, 9, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 0, 0, 0, 0, 0],
         [0, 0, 0, 2, 0, 0, 0, 0, 0],
         [0, 0, 0, 3, 0, 0, 0, 0, 0]])
    print(sudo_test.grid)
    assert_true((sudo_test.grid == sudo_ref).all())


def test_sudoku_block():
    sudo_test = Sudoku(np.array(
        [[1, 2, 3, 0, 0, 0, 0, 0, 0],
         [0, 5, 6, 0, 0, 0, 0, 0, 0],
         [7, 8, 9, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0]]))
    sudo_test.update_mask()
    sudo_test.fill_grid()
    sudo_ref = np.array(
        [[1, 2, 3, 0, 0, 0, 0, 0, 0],
         [4, 5, 6, 0, 0, 0, 0, 0, 0],
         [7, 8, 9, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0]])
    print(sudo_test.grid)
    assert_true((sudo_test.grid == sudo_ref).all())


def test_sudoku_second_order():
    sudo_test = Sudoku(np.array(
        [[1, 2, 0, 0, 0, 0, 0, 0, 0],
         [3, 4, 0, 0, 0, 0, 0, 0, 0],
         [5, 6, 7, 0, 0, 0, 0, 0, 0],
         [0, 8, 0, 0, 0, 0, 0, 0, 0],
         [0, 9, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 9],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [4, 0, 0, 0, 0, 0, 0, 0, 0]]))
    sudo_test.update_mask()
    sudo_test.fill_grid()
    sudo_ref = np.array(
        [[1, 2, 0, 0, 0, 0, 0, 0, 0],
         [3, 4, 0, 0, 0, 0, 0, 0, 0],
         [5, 6, 7, 0, 0, 0, 0, 0, 0],
         [0, 8, 0, 0, 0, 0, 0, 0, 0],
         [0, 9, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [8, 0, 0, 0, 0, 0, 0, 0, 9],
         [9, 0, 0, 0, 0, 0, 0, 0, 0],
         [4, 0, 0, 0, 0, 0, 0, 0, 0]])
    assert_true((sudo_test.grid == sudo_ref).all())


def test_sudoku_second_order_v2():
    sudo_test = Sudoku(np.array(
        [[0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [1, 3, 5, 0, 0, 0, 0, 0, 4],
         [2, 4, 6, 8, 9, 0, 0, 0, 0],
         [0, 0, 7, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 9, 0, 0]]))
    sudo_test.update_mask()
    sudo_test.fill_grid()
    sudo_ref = np.array(
        [[0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [1, 3, 5, 0, 0, 0, 8, 9, 4],
         [2, 4, 6, 8, 9, 0, 0, 0, 0],
         [0, 0, 7, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 9, 0, 0]])
    print(sudo_test.mask[:, :, 6])
    assert_true((sudo_test.grid == sudo_ref).all())


def test_all():
    answer = []
    with open('tests/test_input_large.txt', 'r') as file:
        num = 0
        for line in file:
            if 'Grid' in line:
                counter = 0
                grid = []
            elif counter < 9:
                grid.append([int(x) for x in line.strip()])
                counter += 1
            if counter == 9:
                sudoku_to_solve = Sudoku(np.array(grid))
                answer.append(sudoku_to_solve.solve())
                num += 1
                print(num)
    assert_true(np.array(answer).all())


def test_image_read():
    data = SudokuImage('tests/sudoku.png')
    data.parse()
    target = np.array(
        [[5, 3, 0, 0, 7, 0, 0, 0, 0],
         [6, 0, 0, 1, 9, 5, 0, 0, 0],
         [0, 9, 8, 0, 0, 0, 0, 6, 0],
         [8, 0, 0, 0, 6, 0, 0, 0, 3],
         [4, 0, 0, 8, 0, 3, 0, 0, 1],
         [7, 0, 0, 0, 2, 0, 0, 0, 6],
         [0, 6, 0, 0, 0, 0, 2, 8, 0],
         [0, 0, 0, 4, 1, 9, 0, 0, 5],
         [0, 0, 0, 0, 8, 0, 0, 7, 9]])
    assert_true((data.grid == target).all())
