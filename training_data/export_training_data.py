#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  5 16:26:22 2017

@author: casper

Reads in N 9x9 grids with numbers and the corresponding csv file.
The 9x9 grids are processed the same as Sudoku grids. Raw data is
manually created using LibreOffice Calc using random input and a different
font for each image.
"""

import numpy as np
import pickle
from sudoku_solver.image_parser import SudokuImage

target = np.genfromtxt('targets.csv', delimiter=',', dtype='int')
target = [int(i) for i in np.nditer(target)]

out = []
for i in range(1, 5):
    img = SudokuImage('sudoku_{:02}.png'.format(i))
    img.find_grid()
    img.find_cells()
    img.clean_cells()
    img.clean_cells(trim=False)
    img.resize_cells()
    out.extend(img.cells)
test = []
for i in out:
    test.append(i.reshape(i.size))
data = {'target': target, 'test': test}
pickle.dump(data, open('test_data', 'wb'))
